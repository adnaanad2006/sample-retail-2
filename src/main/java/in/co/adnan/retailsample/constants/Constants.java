package in.co.adnan.retailsample.constants;

public interface Constants {

	int CUTOMER_DISCOUNT_ELLIGIBILITY = 24; // months
	int FIXED_AMOUNT_LIMIT = 100; // months
	int FIXED_AMOUNT_LIMIT_DISCOUNT = 5; // months
}
