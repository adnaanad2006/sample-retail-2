package in.co.adnan.retailsample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import in.co.adnan.retailsample.model.Category;
import in.co.adnan.retailsample.model.Discount;
import in.co.adnan.retailsample.model.Role;
import in.co.adnan.retailsample.model.User;
import in.co.adnan.retailsample.repos.CategoryRepository;
import in.co.adnan.retailsample.repos.DiscountRepository;
import in.co.adnan.retailsample.repos.RoleRepository;
import in.co.adnan.retailsample.services.UserService;

@SpringBootApplication(scanBasePackages = "in.co.adnan.retailsample")
public class RetailSampleApplication implements CommandLineRunner {

	@Autowired
	UserService userService;
	@Autowired
	RoleRepository roleRepo;
	@Autowired
	CategoryRepository categoryRepo;
	@Autowired
	DiscountRepository discountRepo;

	public static void main(String[] args) {
		SpringApplication.run(RetailSampleApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Role employeeRole = new Role(1, "employee");
		Role affiRole = new Role(2, "affiliate");
		Role customerRole = new Role(3, "customer");

		roleRepo.save(employeeRole);
		roleRepo.save(affiRole);
		roleRepo.save(customerRole);
		
		

		Category grocery = new Category(1, "Grocery");
		Category other = new Category(2, "Other");
		categoryRepo.save(grocery);
		categoryRepo.save(other);
		

		User u1 = new User("Employee User", employeeRole);
		userService.addRandomUser(u1);
		User u2 = new User("Affiliate User", affiRole);
		userService.addRandomUser(u2);
		User u3 = new User("Customer User", customerRole);
		userService.addRandomUser(u3);
		
		

		Discount employeeDiscount = new Discount(1, "Employee Discount", 1, 30);
		Discount affiDiscount = new Discount(2, "Affiliate Discount", 1, 10);
		Discount customerDiscount = new Discount(3, "Customer Discount", 1, 5);
		Discount fixedDiscount = new Discount(4, "Fixed Discount", 2, 5);

		discountRepo.save(employeeDiscount);
		discountRepo.save(affiDiscount);
		discountRepo.save(customerDiscount);
		discountRepo.save(fixedDiscount);
		
		
	}
	
	

}
