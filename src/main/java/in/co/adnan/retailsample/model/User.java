package in.co.adnan.retailsample.model;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "users")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	

	private String name;


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false, updatable = false)
    @CreatedDate
    private Date dateCreated;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JsonIgnore
    private Role role;
    
	public User() {
		super();
	}

	

	public User(String name, Role role) {
		super();
		this.name = name;
		this.dateCreated = new Date();
		this.role = role;
	}


	

	public User(int id, String name, Role role) {
		super();
		this.id = id;
		this.name = name;
		this.dateCreated = new Date();
		this.role = role;
	}
	


	

	public User(int id, String name, Role role, Date date) {
		super();
		this.id = id;
		this.name = name;
		this.dateCreated = date;
		this.role = role;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}



	public Role getRole() {
		return role;
	}



	public void setRole(Role role) {
		this.role = role;
	}
	
	
    
    

}
