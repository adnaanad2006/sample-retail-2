package in.co.adnan.retailsample.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "discounts")
public class Discount {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String name;
	
	private int discountType;
	
	private float discount;

	public Discount() {
		super();
	}

	public Discount(int id, String name, int discountType, float discount) {
		super();
		this.id = id;
		this.name = name;
		this.discountType = discountType;
		this.discount = discount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDiscountType() {
		return discountType;
	}

	public void setDiscountType(int discountType) {
		this.discountType = discountType;
	}

	public float getDiscount() {
		return discount;
	}

	public void setDiscount(float discount) {
		this.discount = discount;
	}

	@Override
	public String toString() {
		return "Discount [id=" + id + ", name=" + name + ", discountType=" + discountType + ", discount=" + discount
				+ "]";
	}
	
	
	
	
	
	
}
