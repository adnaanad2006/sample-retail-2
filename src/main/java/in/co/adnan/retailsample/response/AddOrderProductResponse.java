package in.co.adnan.retailsample.response;

public class AddOrderProductResponse {

	private int userId;
	private Double subTotal;
	private Double discount;
	private Double grandTotal;
	
	public AddOrderProductResponse(int user_id, Double sub_total, Double discount, Double grand_total) {
		super();
		this.userId = user_id;
		this.subTotal = sub_total;
		this.discount = discount;
		this.grandTotal = grand_total;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int user_id) {
		this.userId = user_id;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double sub_total) {
		this.subTotal = sub_total;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Double grand_total) {
		this.grandTotal = grand_total;
	}
	
	

}
