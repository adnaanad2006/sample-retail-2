package in.co.adnan.retailsample.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import in.co.adnan.retailsample.model.Order;
import in.co.adnan.retailsample.model.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

}
