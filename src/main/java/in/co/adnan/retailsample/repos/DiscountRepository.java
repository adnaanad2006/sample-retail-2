package in.co.adnan.retailsample.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import in.co.adnan.retailsample.model.Discount;

public interface DiscountRepository extends JpaRepository<Discount, Integer> {

}
