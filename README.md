Model Relation Diagram: 
It is available in the root folder named "uml.png". 

Code Functionality:

-> Post data to exposed URL "{HOST_URL}/orders/addToCart" with user id and list of products
-> API will return user_id, sub_total, discount, grand_total in the data key. 
-> Example response: 

{
    "status": 1,
    "message": "Cart Updated successfully",
    "data": {
        "userId": 1,
        "subTotal": 150.0,
        "discount": 5.0,
        "grandTotal": 145.0
    }
}

-> Discount is calculated according to user type, user_created_date and product category type.
-> User's discount is saved in databse. Fixed Discount is stored in a constant file.
   



Test Cases:

-> 14 test cases has been created in OrderRestControllerTests.java.
-> Used MockBean to provide mock data
-> Checked values for status, data.subTotal, data.discount, data.grandTotal in the response.
-> Checked data key exists in the response.  


1.  addOrderApi_withEmployee_withTotal85_thenReturnJsonArray
2.  addOrderApi_withEmployee_withTotal515_thenReturnJsonArray 
3.  addOrderApi_withEmployee_withTotal995_withGroceryProduct_thenReturnJsonArray
4.  addOrderApi_withEmployee_withTotal995_thenReturnJsonArray
5.  addOrderApi_withAffiliate_withTotal85_thenReturnJsonArray
6.  addOrderApi_withAffiliate_withTotal515_thenReturnJsonArray
7.  addOrderApi_withAffiliate_withTotal995_thenReturnJsonArray
8.  addOrderApi_withCustomer_withTotal85_thenReturnJsonArray
9.  addOrderApi_withCustomer_withTotal515_thenReturnJsonArray
10. addOrderApi_withCustomer_withTotal995_thenReturnJsonArray
11. addOrderApi_withOldCustomer_withTotal85_thenReturnJsonArray
12. addOrderApi_withOldCustomer_withTotal515_thenReturnJsonArray
13. addOrderApi_withOldCustomer_withTotal995_thenReturnJsonArray
14. addOrderApi_withOldCustomer_withTotal995_withGrocery_thenReturnJsonArray

-> 14 Test completed without any failure


Build Project Using Command Line:

run command from root of the project:

> mvn clean install

A jar file will be created into the target folder

Example: \target\retail-sample-0.0.1-SNAPSHOT.jar



Run Jar file from the root (Command prompt)

> java -jar target/retail-sample-0.0.1-SNAPSHOT.jar


Sonarqube: 

	Added a plugin for sonar scanning
	Download and extract Sonar in a folder
	and run sonarqube\bin\windows-x86-xx\StartSonar.bat
	Login into the sonar "http://localhost:9000" and generate a token for the project
	run following command from root of the project
	
> mvn sonar:sonar -Dsonar.host.url=http://localhost:9000 -Dsonar.login=the-generated-token

Please have a look at the screenshot "sonarqube.png"
	 


Code Coverage:

	Code Coverage Test Using EclEmma in STS: Total Code is 89.0% see screenshot on the root "coverage.png" 





 







 